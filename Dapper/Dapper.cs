﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace Dapper
{
    public partial struct CommandDefinition
    {
        private static SqlMapper.Link<Type, Action<IDbCommand>> commandInitCache;
    }
    public sealed partial class DbString
    {
        /// <summary>
        /// Default value for IsAnsi.
        /// </summary>
        public static bool IsAnsiDefault { get; set; }
    }
    public sealed partial class DefaultTypeMap
    {
        // DefaultTypeMap
        /// <summary>
        /// Should column names like User_Id be allowed to match properties/fields like UserId ?
        /// </summary>
        public static bool MatchNamesWithUnderscores { get; set; }
    }
    public partial class DynamicParameters
    {
        // The type here is used to differentiate the cache by type via generics
        // ReSharper disable once UnusedTypeParameter
        internal static class CachedOutputSetters<T>
        {
            // Intentional, abusing generics to get our cache splits
            // ReSharper disable once StaticMemberInGenericType
            public static readonly Hashtable Cache = new Hashtable();
        }
    }
    public partial class DynamicParameters
    {
        private static readonly Dictionary<SqlMapper.Identity, Action<IDbCommand, object>> paramReaderCache = new Dictionary<SqlMapper.Identity, Action<IDbCommand, object>>();
    } 

    public static partial class SqlMapper
    {
        /// <summary>
        /// Called if the query cache is purged via PurgeQueryCache
        /// </summary>
        public static event EventHandler QueryCachePurged;

        private static readonly System.Collections.Concurrent.ConcurrentDictionary<Identity, CacheInfo> _queryCache = new System.Collections.Concurrent.ConcurrentDictionary<Identity, CacheInfo>();

        private static int collect;

        private static Dictionary<Type, DbType> typeMap;

        private static Dictionary<Type, ITypeHandler> typeHandlers;

        private static readonly int[] ErrTwoRows = new int[2], ErrZeroRows = new int[0];

        /// <summary>
        /// Gets type-map for the given type
        /// </summary>
        /// <returns>Type map instance, default is to create new instance of DefaultTypeMap</returns>
        public static Func<Type, ITypeMap> TypeMapProvider = (Type type) => new DefaultTypeMap(type);

        // use Hashtable to get free lockless reading
        private static readonly Hashtable _typeMaps = new Hashtable();

        private static IEqualityComparer<string> connectionStringComparer = StringComparer.Ordinal;

        // one per thread
        [ThreadStatic]
        private static StringBuilder perThreadStringBuilderCache;

        /// <summary>
        /// Permits specifying certain SqlMapper values globally.
        /// </summary>
        public static partial class Settings
        {
            internal static CommandBehavior AllowedCommandBehaviors { get; private set; } = DefaultAllowedCommandBehaviors;


            /// <summary>
            /// Specifies the default Command Timeout for all Queries
            /// </summary>
            public static int? CommandTimeout { get; set; }

            /// <summary>
            /// Indicates whether nulls in data are silently ignored (default) vs actively applied and assigned to members
            /// </summary>
            public static bool ApplyNullValues { get; set; }

            /// <summary>
            /// Should list expansions be padded with null-valued parameters, to prevent query-plan saturation? For example,
            /// an 'in @foo' expansion with 7, 8 or 9 values will be sent as a list of 10 values, with 3, 2 or 1 of them null.
            /// The padding size is relative to the size of the list; "next 10" under 150, "next 50" under 500,
            /// "next 100" under 1500, etc.
            /// </summary>
            /// <remarks>
            /// Caution: this should be treated with care if your DB provider (or the specific configuration) allows for null
            /// equality (aka "ansi nulls off"), as this may change the intent of your query; as such, this is disabled by 
            /// default and must be enabled.
            /// </remarks>
            public static bool PadListExpansions { get; set; }
            /// <summary>
            /// If set (non-negative), when performing in-list expansions of integer types ("where id in @ids", etc), switch to a string_split based
            /// operation if there are more than this many elements. Note that this feautre requires SQL Server 2016 / compatibility level 130 (or above).
            /// </summary>
            public static int InListStringSplitCount { get; set; } = -1;
        }

        private partial class TypeDeserializerCache
        {
            private static readonly Hashtable byType = new Hashtable();
        }

        public static partial class TypeHandlerCache<T>
        {
            private static ITypeHandler handler;
        }
    }
    /// <summary>
    /// Used to pass a DataTable as a TableValuedParameter
    /// </summary>
    internal sealed partial class TableValuedParameter
    {
        private static readonly Action<System.Data.SqlClient.SqlParameter, string> setTypeName;
    }
}
